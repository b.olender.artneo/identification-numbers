(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.IdentificationNumbers = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
exports.regon = require('./regon.js').regon;
exports.pesel = require('./pesel.js').pesel;
exports.nip = require('./nip.js').nip;
},{"./nip.js":2,"./pesel.js":3,"./regon.js":4}],2:[function(require,module,exports){
var util = require('./util.js');

exports.nip = function (value) {

    var WEIGHTS = [6, 5, 7, 2, 3, 4, 5, 6, 7],
        MODULO = 11;

    var Nip = function (value) {
        this.value = value;
    };

    Nip.prototype.isValid = function () {
        if (util.isString(this.value) && this.value.length === 10) {
            var i, sum = 0, checksum, digits = [];
            for (i = 0; i < WEIGHTS.length; i++) {
                digits.push(+this.value[i]);
                sum += digits[i] * WEIGHTS[i];
            }
            checksum = sum % MODULO;
            return checksum === (+this.value[9]);
        }
        return false;
    };

    Nip.prototype.random = function () {
        var i, sum = 0, checksum, digits = [];

        for (i = 0; i < WEIGHTS.length; i++) {
            digits.push(util.randomInt(1, 9));
            sum += digits[i] * WEIGHTS[i];
        }

        checksum = sum % MODULO;
        if (checksum === 10 || checksum === 0) {
            return this.random();
        }

        return digits.join('') + checksum;
    };

    return new Nip(value);

};
},{"./util.js":5}],3:[function(require,module,exports){
var util = require('./util.js');

exports.pesel = function (value) {

    var WEIGHTS = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3],
        LENGTH = 11,
        MODULO = 10;

    var Pesel = function (value) {
        this.value = value;
    };

    Pesel.prototype.decode = function () {
        return {
            ordinal: +this.value.substring(6, 10),
            checksum: +this.value[10],
            sex: (this.value[9] % 2 === 0 ? 'F' : 'M')
        };
        // TODO YEAR, MONTH, DAY
    };

    Pesel.prototype.isValid = function () {
        if (util.isString(this.value) && this.value.length === LENGTH) {
            var i, sum = 0, checksum, digits = [];
            for (i = 0; i < WEIGHTS.length; i++) {
                digits.push(+this.value[i]);
                sum += digits[i] * WEIGHTS[i];
            }
            checksum = (10 - sum % MODULO) % 10;
            return checksum === (+this.value[LENGTH - 1]);
        }
        return false;
    };

    Pesel.prototype.random = function () {
        // FIXME Implement this guy
        return 'ABC';
    };

    return new Pesel(value);

};
},{"./util.js":5}],4:[function(require,module,exports){
var util = require('./util.js');

exports.regon = function (value) {

    var WEIGHTS = [8, 9, 2, 3, 4, 5, 6, 7],
        MODULO = 11;

    var Regon = function (value) {
        this.value = value;
    };

    Regon.prototype.random = function () {
        var i, sum = 0, checksum, digits = [];

        for (i = 0; i < WEIGHTS.length; i++) {
            digits.push(util.randomInt(1, 9));
            sum += digits[i] * WEIGHTS[i];
        }

        checksum = (sum % MODULO) % 10;
        return digits.join('') + checksum;
    };

    Regon.prototype.isValid = function () {
        if (util.isString(this.value) && this.value.length === 9) {
            var i, sum = 0, checksum, digits = [];
            for (i = 0; i < WEIGHTS.length; i++) {
                digits.push(+this.value[i]);
                sum += digits[i] * WEIGHTS[i];
            }
            checksum = sum % MODULO;
            return checksum === (+this.value[8]);
        }
        return false;
    };

    return new Regon(value);

};
},{"./util.js":5}],5:[function(require,module,exports){
exports.randomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.isString = function (value) {
    return typeof value === 'string'
};
},{}]},{},[1])(1)
});
